package model.vo;

import java.util.Iterator;

public class Bike implements Comparable<Bike>{

    private int bikeId;
    private int totalTrips;
    private int totalDistance;
    private int totalDuration;

    public Bike(int bikeId, int totalTrips, int totalDistance, int ptotalDuration) {
        this.bikeId = bikeId;
        this.totalTrips = totalTrips;
        this.totalDistance = totalDistance;
        this.totalDuration = ptotalDuration;
    }

    @Override
    public int compareTo(Bike o) {
    	// TODO completar
        return 0;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTotalTrips() {
        return totalTrips;
    }

    public int getTotalDistance() {
        return totalDistance;
    }
    public int getTotalDuration() {
    	return totalDuration;
    }
}
